# coding=utf-8

import csv
from multiprocessing import Pool

import random
import numpy as np

from lib.soliton import Soliton


__author__ = 'Mattia Bertorello'


class RatelessCode:
    def __init__(self, random_func):
        self.random_func = random_func

    def encoder(self, N, source_array):
        # Lunghezza del vettore sorgente
        K = len(source_array)
        # Inizializzo i paccetti in uscita
        encoded_packets = np.zeros(N, dtype=np.uint32)
        # Inizializzo la matrice G che mi serve per sapere di quali pacchetti ho fatto lo xor
        g_matrix = np.zeros(((N, K)), dtype=np.bool)
        # Itero per N pacchetti che devo generare
        for encoded_packet_index in range(N):
            # Genero il numero casuale del grado di codifica
            d = self.random_func()
            # Genero una serie di indici di numero d tutti diversi
            d_indexes = random.sample(range(K), d)
            #print d_indexes
            # Eseguo lo xor per trovare il paccetto da spedire
            ris = 0
            for d_index in d_indexes:
                ris = np.bitwise_xor(ris, source_array[d_index])
                g_matrix[encoded_packet_index, d_index] = True

            # Creo il paccetto codificato
            encoded_packets[encoded_packet_index] = ris

        return encoded_packets, g_matrix

    def decoder(self, encoded_packets, g_matrix):
        # Decoder
        N, K = g_matrix.shape

        # Inizializzo il vettore di pacchetti decodificati
        decoded_packets = np.zeros(K, dtype=np.uint32)
        # Vado avanti finchè non trovo più pacchetti da decodificare
        one_degree_y = -1
        index = 0
        # Trovo gli indici di una colonna di grado 1
        while index < N and one_degree_y == -1:
            # Esiste un pacchetto di grado 1
            if g_matrix[index].sum() == 1:
                # Indide del pacchetto già decodificato ma nell'array dei pacchetti codificati
                one_degree_y = index
            index += 1
        while True:

            # Indice del pacchetto decodificato
            one_degree_x = np.where(g_matrix[one_degree_y])

            # Se non trovo più pacchetti con grado 1
            if one_degree_y == -1:
                break

            # Ho risolto questa equazione quindi metto come risolta
            g_matrix[one_degree_y][one_degree_x] = False
            # Inserisco il pacchetto decodificato nell'array dei pacchetti decodificati
            decoded_packets[one_degree_x] = encoded_packets[one_degree_y]

            one_degree_y = -1
            # Rimuovo il pacchetto decodificato dall'array dei pacchetti da decodificare
            for i in range(N):
                if g_matrix[i][one_degree_x]:
                    encoded_packets[i] = np.bitwise_xor(encoded_packets[i], decoded_packets[one_degree_x])
                    g_matrix[i][one_degree_x] = False
                if g_matrix[i].sum() == 1:
                    one_degree_y = i

        return decoded_packets


def test_rateless_coder(xs, n, random_func):
    rateless_coder = RatelessCode(random_func)
    encoded_packets, g_matrix = rateless_coder.encoder(n, xs)
    decoded_packets = rateless_coder.decoder(encoded_packets, g_matrix)
    return np.array_equal(decoded_packets, xs)


def test_with_worker(params):
    print "Worker "
    TIMES, xs, k_times, k, c, delta = params
    soliton = Soliton(k, c, delta, random_generator)
    # Numero di pacchetti
    n = int((k_times / 10.0 + 1) * k)
    print k, n
    test_result = 0.0
    for times in range(TIMES):
        xs = np.array(np.random.randint(0, 2 ** 32, size=k), dtype=np.uint32)
        test_result += test_rateless_coder(xs, n, soliton.generate)
    print k, k_times / 10.0 + 1, test_result / TIMES
    return k, k_times / 10.0 + 1, test_result / TIMES


def test_with_worker_no_soliton(params):
    print "Worker "
    TIMES, xs, k_times, k = params
    random_generator = np.random.RandomState()
    soliton = Soliton(k, c, delta, random_generator)

    def generator():
        # Provare con ideal soliton e uniforme con n grande
        #random_generator.exponential(0.5) % k
        #int(random_generator.uniform(1, k/6))
        return soliton.ideal_soliton();

    # Numero di pacchetti
    n = int((k_times / 10.0 + 1) * k)
    print k, n
    test_result = 0.0
    for times in range(TIMES):
        test_result += test_rateless_coder(xs, n, generator)
        print times, test_result
    print k, k_times / 10.0 + 1, test_result / TIMES
    return k, k_times / 10.0 + 1, test_result / TIMES


if __name__ == "__main__":
    print 'Rateless Code'
    # Lunghezze di numeri letti
    ks = [50, 100, 200, 500]
    # Parametri del generatore di Soliton
    c = 0.05
    delta = 0.05
    random_generator = np.random.RandomState()
    TIMES = 100
    pool = Pool()
    params = []
    for k in ks:
        xs = 0
        for k_times in range(1, 11):
            params.append((TIMES, xs, k_times, k, c, delta))

    results = pool.map(test_with_worker, params)
    print results
    with open('test_rateless_1.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(results)
        # Test con diverse distribuzioni
        # ks = [500]
        # params = []
        # for k in ks:
        #     xs = np.array(np.random.randint(0, 2 ** 32, size=k), dtype=np.uint32)
        #     for k_times in range(1, 11):
        #         params.append((TIMES, xs, k_times, k))
        #
        # results = pool.map(test_with_worker_no_soliton, params)
        # print results
        # with open('test_rateless_2.csv', 'wb') as f:
        #     writer = csv.writer(f)
        #     writer.writerows(results)




