from math import log

__author__ = 'Mattia Bertorelo'

from itertools import tee
import matplotlib.pyplot as plt

from lib import bit_generators as generators
from lib import source as s

# P(x) = numero di symboli / numero totale di symboli
# somma di tutte le P(x) * log p(x)
# Grafico di Rapporto di compresessione al variare di N
# Grafico H(x) al variare di N


def calculateEntropyAndCompressionRate(symbolLengthsList, bitsGenerator):
    generators = tee(bitsGenerator.bits(), len(symbolLengthsList))
    zipLengthsAndGenerators = zip(symbolLengthsList, generators)
    result = []
    for length, generator in zipLengthsAndGenerators:
        sourceSymbols = s.Symbols(length)
        symbolsOccurrences = sourceSymbols.createDictionaryOccurrences(generator)
        sourceProperties = s.SourceProperties(symbolsOccurrences)
        entropy = sourceProperties.normalizeEntropy()
        compressionRate = sourceProperties.compressionRate()
        print (length, entropy, compressionRate)
        result.append((length, entropy, compressionRate))
    return result


def drawPlot(data, fileName):
    fig = plt.figure()
    plt.xlabel('words length', fontsize=14, color='red')
    plt.title('Entrpy & Compression Rate: ')
    plt.grid(True)
    #plt.plot(data[0], data[2], 'r', label='Compression Rate')
    plt.plot(data[0], data[1], 'g-*', label='Entropy')
    plt.legend(loc='best')
    plt.ylim(min(min(data[1]), min(data[1])), max(max(data[1]), max(data[1])))
    plt.show()
    fig.savefig(fileName + '_entropy.png', bbox_inches=0)


def drawPlotGenerateBit(data, data_zero, true_entropy, fileName):
    fig = plt.figure()
    plt.xlabel('words length', fontsize=14, color='red')
    plt.title('Entrpy & Compression Rate: ')
    plt.grid(True)
    #plt.plot(data[0], data[2], 'r', label='Compression Rate')
    plt.plot(data_zero, true_entropy, 'r', label='True Entropy')
    for data_with__fixed_bits in data:
        data_with__fixed_probability = zip(*data_with__fixed_bits[1])
        plt.plot(data_zero, data_with__fixed_probability[1], 'g-*',
                 label='Entropy with {0} bits'.format(data_with__fixed_bits[0]))
    plt.legend(loc='best')
    #plt.ylim(min(min(data[1]), min(data[2])), max(max(data[1]), max(data[2])))
    plt.show()
    fig.savefig(fileName + '_entropy.png', bbox_inches=0)


def theory_entropy(path, symbolLengthList):
    probabilities = [0.5]#, 0.6, 0.8, 0.99]
    numbersOfBits = [100, 1000, 10000, 10000000]
    for probability in probabilities:
        data = []
        for numberOfBits in numbersOfBits:
            bitsGenerator = generators.RandomBitsGenerator(probability, numberOfBits + 1)
            aux = calculateEntropyAndCompressionRate(symbolLengthList, bitsGenerator)
            data.append((numberOfBits, aux, ))
        entropy = s.SourceProperties.simple_entropy_with_probability([probability, 1 - probability])
        true_entropy = [entropy] * len(symbolLengthList)
        drawPlotGenerateBit(data, symbolLengthList, true_entropy, path + "fixed_probability_" + str(probability))


def file_entropy(path, symbolLengthList):
    files = ['immagine.png', 'immagine.jpg', 'text_file']
    files_path = "test_files/"
    for file in files:
        bitsGenerator = generators.FileBitsGenerator(files_path + file)
    print "File name: " + file
    fileEntropy = {'nameFile': file}
    data = calculateEntropyAndCompressionRate(symbolLengthList, bitsGenerator)
    drawPlot(zip(*data), path + file)

# Contare le sequenze tipiche e calcolare l'entropia
# Prendere il file di testo e critografarlo e vedere cosa succede
#numberOfBits = 10000
#bitsGenerator = RandomBitsGenerator(0.5, numberOfBits)
if __name__ == "__main__":
    #TODO Per i File zippati inserire la linea teorica di zip e vedere cosa noi riusciamo a fare
    path = 'graphs/es1_entropy/'
    maxSymbolLength = 16
    symbolLengthList = [2 ** i for i in xrange(0, int(log(maxSymbolLength, 2) + 1))]
    #symbolLengthList = list(range(2, maxSymbolLength + 1, 2))
    #files = [f for f in listdir(files_path) if isfile(join(files_path, f))]
    data = []
    theory_entropy(path, symbolLengthList)
    file_entropy(path, symbolLengthList)

