# coding=utf-8
from math import log

import matplotlib.pyplot as plt
import numpy as np
from lib import entropy as e

from lib.source import SourceProperties

__author__ = 'Mattia Bertorello'

# Decido l'intervallo centrale tra 0 e 1
# Prendo un simbolo dal generatore di bit
# Se è 0 prendo l'intervallo di sx se è 1 quello di dx
# Aggiorno i nuovi estremi dell'intervallo
# Io mi tengo i due estremi che sono P0 e P1
#   ma anche il punto centrale in percentuale Pi
# Se voglio aggiornare l'indice P1(dx) quando ho 0 faccio Pi=P1*Pi
# Se voglio aggiornare l'indice P0(sx) quando ho 1 faccio P0=P0*Pi
#TODO Fare la codifica non adattativa per vedere dove si schiata

from lib.bit_generators import RandomBitsGenerator


def arithmetic_coding(sources):
    bit_emitted, total, with_interval = [0] * 3

    efficiencies = []
    module_number = 5
    lower_P0, top_P1 = 0.0, 1.0
    for (source_model, bit, probability) in sources:
        middle_point = lower_P0 + ((top_P1 - lower_P0) * probability)
        total += 1
        if bit == 0:
            top_P1 = middle_point
        else:
            lower_P0 = middle_point

        with_interval = top_P1 - lower_P0

        if top_P1 <= 0.5 or lower_P0 > 0.5 or with_interval < 1e-10:
            source_model.bit_emit(with_interval)
            bit_emitted += 1
            if lower_P0 > 0.5:
                lower_P0 -= 0.5
                top_P1 -= 0.5
            if with_interval < 1e-10:
                print 'lower_P0 < 0.5 and top_P1 > 0.5'
                print 'Underflow on ' + str(total)
                print 'Lower_probability= ' + str(lower_P0)
                print 'Top _probability=' + str(top_P1)
                raise Exception()
            lower_P0 *= 2.0
            top_P1 *= 2.0
        source_model.add_result_coding_data(with_interval)
        source_model.lower = lower_P0
        source_model.top = top_P1

        if total % module_number == 0:
            length_interval = bit_emitted + round(log(1 / with_interval, 2)) + 1
            efficiency = length_interval / total
            efficiencies.append((total, efficiency))

    return efficiencies
    #
    # return (efficiency)
    # print 'Length= ' + str(length_interval)
    # print 'with_interval= ' + str(with_interval)
    # print 'bit_emitted= ' + str(bit_emitted)
    # print 'i = ' + str(total)
    # print 'Efficiency= ' + str(efficiency)
    #
    # print 'P0=' + str(lower_P0)
    # print 'P1=' + str(top_P1)


class Probability(object):
    def __init__(self, probability=0):
        self.probability = probability
        self.total = 0

    def add_bit(self, bit):
        self.total += 1
        return self

    def get_probability(self):
        return self.probability


class AdaptiveProbability(Probability):
    def __init__(self):
        super(AdaptiveProbability, self).__init__()
        self.total_zero = 0

    def add_bit(self, bit):
        super(AdaptiveProbability, self).add_bit(bit)
        if bit == 0:
            self.total_zero += 1
        return self

    def get_probability(self):
        return (1.0 + self.total_zero) / (2.0 + self.total)


class Source:
    def __init__(self, probability_zero, number_bits, number_results, type_probability):
        self.probability_zero = probability_zero
        self.number_bits = number_bits
        self.module_number_results = number_bits / number_results
        self.type_probability = type_probability
        self.efficiencies = []
        self.bit_emitted = 0
        self.lower = 0
        self.top = 1

    def get_iterator(self):
        return RandomBitsGenerator(self.probability_zero, self.number_bits).bits()

    def add_result_coding_data(self, with_interval):
        total_symbols = self.type_probability.total
        if total_symbols % self.module_number_results == 0:
            length_interval = self.bit_emitted + round(log(1 / with_interval, 2)) + 1
            efficiency = length_interval / total_symbols
            self.efficiencies.append((total_symbols, efficiency, self.type_probability.get_probability()))

    def bit_emit(self, value):
        self.bit_emitted += 1

    def get_data_for_plot(self):
        entropy = SourceProperties.simple_entropy_with_probability([self.probability_zero, 1 - self.probability_zero])
        efficiencies = zip(*self.efficiencies)
        print 'Entropy= ' + str(entropy) + 'Probability= ' + str(self.probability_zero)
        efficiencies.append([entropy] * len(efficiencies[0]))
        return efficiencies


class Sources:
    def __init__(self):
        self.sources = []

    def add_source(self, source):
        self.sources.append(source)

    def get_iterator(self, iterators_time):
        sources = map(lambda s: [s[0], s[0].get_iterator(), s[1], True, s[0].type_probability],
                      zip(self.sources, iterators_time))
        not_finish_index = 3
        iterator_times_index = 2
        while reduce(lambda ss, ris: ss or ris,
                     map(lambda s: s[not_finish_index], sources), False):
            for source in sources:
                if source[not_finish_index]:
                    try:
                        for i in xrange(source[iterator_times_index]):
                            bit = source[1].next()
                            yield (source[0], bit, source[4].add_bit(bit).get_probability(),)
                    except StopIteration:
                        source[not_finish_index] = False


def drawPlot(data, fileName):
    fig = plt.figure(figsize=(15, 15))
    plt.xlabel('words length', fontsize=14, color='red')
    plt.title('Efficiency')
    plt.grid(True)
    plt.plot(data[0], data[2], 'r', label='Entropy' )
    plt.plot(data[0], data[1], 'g', label='Efficiency' )

    plt.legend(loc='best')
    plt.ylim(min(min(data[1]), min(data[2])), max(max(data[1]), max(data[2])))
    fig.savefig(fileName + '_arithmetic_coding.png', bbox_inches='tight')


def drawPlot2(data, data1, fileName):
    fig = plt.figure(figsize=(15, 15))
    plt.xlabel('words length', fontsize=14, color='red')
    plt.title('Efficiency')
    plt.grid(True)
    plt.plot(data[0], data[2], 'g', label='Entropy  1')
    plt.plot(data[0], data[1], 'g', label='Efficiency 1')
    plt.plot(data[0], data1[1], 'b', label='Efficiency 2')
    plt.plot(data[0], data1[2], 'r', label='Entropy 2')

    plt.legend(loc='best')
    plt.ylim(min(min(data[1]), min(data[2]), min(data1[1])) - 0.2,
             max(max(data[1]), max(data[2]), max(data1[1])) + 0.1)
    fig.savefig('graphs/es2_arithmetic/' + fileName + '.svg', format='svg', bbox_inches='tight')

# Codifica aritmentica ad una sola sorgente con probabilità prefissata
def first_experiment():
    probabilities_of_zero = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99]
    for probability_of_zero in probabilities_of_zero:
        numberOfBits = 10000
        number_of_point = 1000
        fixed_probability = Probability(probability_of_zero)
        source_one = Source(probability_of_zero, numberOfBits, number_of_point, fixed_probability)
        sources = Sources()
        sources.add_source(source_one)
        arithmetic_coding(sources.get_iterator([1]))
        data_plot_1 = source_one.get_data_for_plot()

        fig = plt.figure(figsize=(7, 7))
        plt.xlabel('Numero di simboli codificati')
        plt.title('Efficienza della codifica aritmetica con la probabilita nota')
        plt.grid(True)
        plt.plot(data_plot_1[0], data_plot_1[2], 'r', label='Entropia teorica')
        plt.plot(data_plot_1[0], data_plot_1[1], 'b', label='Efficienza con probabilita fissa')

        a = np.array(data_plot_1[1])
        median = np.median(a)

        data_1 = filter(lambda x: abs(x - median) < 0.1, data_plot_1[1])

        plt.ylim((min(min(data_1), min(data_plot_1[2]))) - 0.1,
                 (max(max(data_1), max(data_plot_1[2]))) + 0.1)
        plt.legend(loc='best')

        fig.savefig('graphs/es2_arithmetic/first_experiment_p_' + str(probability_of_zero) + '.svg', format='svg',  bbox_inches='tight')


def second_experiment():
    probabilities_of_zero = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99]
    numberOfBits = 10000
    number_of_point = 1000
    for probability_of_zero in probabilities_of_zero:
        adaptive_probability = AdaptiveProbability()
        source_one = Source(probability_of_zero, numberOfBits, number_of_point, adaptive_probability)
        sources = Sources()
        sources.add_source(source_one)
        arithmetic_coding(sources.get_iterator([1]))

        data_plot_1 = source_one.get_data_for_plot()
        fig = plt.figure(figsize=(7, 7))
        plt.xlabel('Numero di simboli codificati')
        plt.title('Efficienza della codifica aritmetica con la probabilita adattiva')
        plt.grid(True)
        plt.plot(data_plot_1[0], data_plot_1[2], 'r', label='Entropia teorica')
        plt.plot(data_plot_1[0], data_plot_1[1], 'b', label='Efficienza con probabilita adattiva')

        a = np.array(data_plot_1[1])
        median = np.median(a)

        plt.legend(loc='best')
        data_1 = filter(lambda x: abs(x - median) < 0.1, data_plot_1[1])

        plt.ylim((min(min(data_1), min(data_plot_1[2]))) - 0.1,
                 (max(max(data_1), max(data_plot_1[2]))) + 0.1)
        plt.legend(loc='best')

        fig.savefig('graphs/es2_arithmetic/second_experiment_p_' + str(probability_of_zero) + '.svg', format='svg',  bbox_inches='tight')


def third_experiment():
    probabilities_of_zero = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99]
    numberOfBits = 10000
    number_of_point = 1000
    for probability_of_zero in probabilities_of_zero:
        adaptive_probability = AdaptiveProbability()
        source_one = Source(probability_of_zero, numberOfBits, number_of_point, adaptive_probability)
        sources = Sources()
        sources.add_source(source_one)
        arithmetic_coding(sources.get_iterator([1]))
        fixed_probability = Probability(probability_of_zero)
        source_two = Source(probability_of_zero, numberOfBits, number_of_point, fixed_probability)
        sources = Sources()
        sources.add_source(source_two)
        arithmetic_coding(sources.get_iterator([1]))
        data_plot_1 = source_one.get_data_for_plot()
        data_plot_2 = source_two.get_data_for_plot()
        fig = plt.figure(figsize=(7, 7))
        plt.xlabel('Numero di simboli codificati')

        plt.title('Efficienza della codifica aritmetica con confronto della probabilita adattiva e fissa')
        plt.grid(True)
        plt.plot(data_plot_1[0], data_plot_1[2], 'r', label='Entropia teorica')
        plt.plot(data_plot_1[0], data_plot_1[1], 'b', label='Efficienza con probabilita adattiva')
        plt.plot(data_plot_1[0], data_plot_2[1], 'g', label='Efficienza con probabilita fissa')

        a = np.array(data_plot_1[1])
        b = np.array(data_plot_1[2])
        median = np.median([a, b])

        plt.legend(loc='best')
        data_1 = filter(lambda x: abs(x - median) < 0.1, data_plot_1[1])
        data_2 = filter(lambda x: abs(x - median) < 0.1, data_plot_2[1])

        plt.ylim((min(min(data_1), min(data_plot_1[2]), min(data_2))) - 0.1,
                 (max(max(data_1), max(data_plot_2[2]), max(data_2))) + 0.1)
        fig.savefig('graphs/es2_arithmetic/third_experiment_p_' + str(probability_of_zero) + '.svg', format='svg',  bbox_inches='tight')


def fourth_experiment():
    probabilities_of_zero = [(0.6, 0.9), (0.7,0.8), (0.5, 0.99)]
    numberOfBits = 10000
    number_of_point = 1000
    times_for_each_source = [1, 1]
    for probability_of_zero in probabilities_of_zero:
        p1 = AdaptiveProbability()
        p2 = AdaptiveProbability()
        source_one = Source(probability_of_zero[0], numberOfBits, number_of_point, p1)
        source_two = Source(probability_of_zero[1], numberOfBits, number_of_point, p2)
        sources = Sources()
        sources.add_source(source_one)
        sources.add_source(source_two)
        efficiencies = arithmetic_coding(sources.get_iterator(times_for_each_source))

        data_plot_1 = source_one.get_data_for_plot()
        data_plot_2 = source_two.get_data_for_plot()
        p_zero_0 = probability_of_zero[0]
        p_zero_1 = probability_of_zero[1]
        n_source_1 = source_one.type_probability.total
        n_source_2 = source_two.type_probability.total
        print n_source_1, p_zero_0, e.theory_entropy([p_zero_0, 1 - p_zero_0])
        print n_source_2, p_zero_1, e.theory_entropy([p_zero_1, 1 - p_zero_1])
        #median_entropy = ((n_source_1 * e.theory_entropy([p_zero_0, 1 - p_zero_0])) + (n_source_2 * e.theory_entropy([p_zero_1, 1 - p_zero_1]))) / (n_source_2 + n_source_2)

        print efficiencies
        print source_one.type_probability.get_probability()
        print source_two.type_probability.get_probability()


        # b = []
        # i = 0
        # for a in zip(data_plot_1[1], data_plot_2[1]):
        #     i += 1
        #     ris = (a[0] + a[1]) / i
        #     b.append(ris)
        # print b
        data_plot = zip(*efficiencies)
        median_entropy = np.median(data_plot[1])
        print "Media Entropia: {}".format(median_entropy)
        fig = plt.figure(figsize=(12, 12))
        plt.xlabel('Numero di simboli codificati')
        plt.title('Efficienza della codifica aritmetica con due sorgenti')
        plt.grid(True)
        #plt.plot(data_plot_1[0], data_plot_1[2], 'r', label='Entropy Source One P=' + str(probability_of_zero[0]))
        #plt.plot(data_plot_1[0], data_plot_2[2], 'c', label='Entropy Source Two P=' + str(probability_of_zero[1]))
        plt.plot(data_plot[0], [median_entropy] * len(data_plot[0]), 'r', label="Media pesata dell'entropia delle sorgenti")

        plt.plot(data_plot[0], [e.theory_entropy([p_zero_0, 1 - p_zero_0])] * len(data_plot[0]), 'k', label="Entropia Sorgente 1")
        plt.plot(data_plot[0], [e.theory_entropy([p_zero_1, 1 - p_zero_1])] * len(data_plot[0]), 'b', label="Entropia Sorgente 2")

        plt.plot(data_plot_1[0], data_plot_1[2], 'c', label="Probabilita' Sorgente 1");

        plt.plot(data_plot_2[0], data_plot_2[2], 'm', label="Probabilita' Sorgente 2")

        #plt.plot(data_plot_1[0], data_plot_1[1], 'g', label='Efficiency Source One P=' + str(probability_of_zero[0]))
        #plt.plot(data_plot_1[0], data_plot_2[1], 'b', label='Efficiency Source Two P=' + str(probability_of_zero[1]))

        plt.plot(data_plot[0], data_plot[1], 'g', label="Media pesata dell'efficienza delle due sorgenti")



        # a = np.array(data_plot_1[1])
        # b = np.array(data_plot_1[2])
        # median_a = np.median(a)
        # median_b = np.median(b)
        plt.legend(loc='best')
        #
        # data_1 = filter(lambda x: abs(x - median_a) < 0.5, data_plot_1[1])
        # data_2 = filter(lambda x: abs(x - median_b) < 0.5, data_plot_2[1])
        #
        # plt.ylim((min(min(data_1), min(data_plot_1[2]), min(data_2))) - 0.3,
        #          (max(max(data_1), max(data_plot_2[2]), max(data_2))) + 0.1)
        fig.savefig('graphs/es2_arithmetic/fourth_experiment_p_' + str(probability_of_zero) + '.svg', format='svg', bbox_inches='tight')


def simulate_arithmetic_coding():
    #First Experiment
    #first_experiment()
    #second_experiment()
    #third_experiment()
    fourth_experiment()
    return
    probabilityOfZero = 0.6
    numberOfBits = 100000
    take_number = 100
    p1 = Probability(probabilityOfZero)
    p2 = Probability(0.9)
    source_one = Source(probabilityOfZero, numberOfBits, take_number, p1)
    source_two = Source(0.9, 10000, take_number, p2)
    sources = Sources()
    sources.add_source(source_one)
    sources.add_source(source_two)
    efficiencies = arithmetic_coding(sources.get_iterator([1, 1]))

    entropy = SourceProperties.simple_entropy_with_probability([probabilityOfZero, 1 - probabilityOfZero])
    efficiencies = zip(*efficiencies)
    efficiencies.append([entropy] * len(efficiencies[0]))
    drawPlot(efficiencies, 'not_adaptive')

    drawPlot(source_one.get_data_for_plot(), 'source_one')

    drawPlot(source_two.get_data_for_plot(), 'source_two')

    drawPlot2(source_one.get_data_for_plot(), source_two.get_data_for_plot(), 'source_one and two')

    print 'Entropy= ' + str(entropy)


if __name__ == '__main__':
    simulate_arithmetic_coding()


    # ps =np.linspace(0.0, 1.0, num=10, endpoint=True)
    # entropies = []
    #
    # for p in ps:
    #     entropies.append(SourceProperties.simple_entropy_with_probability([p, 1 - p]))
    #
    # fig = plt.figure()
    # plt.xlabel('probability', fontsize=14, color='red')
    # plt.title('Entropy')
    # plt.grid(True)
    # plt.plot(ps, entropies, 'r-*', label='Entropy')
    #
    # plt.legend(loc='best')
    # plt.ylim(min(entropies), max(entropies))
    # plt.show()
    # fig.savefig('entropy.png', bbox_inches=0)
