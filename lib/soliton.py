from multiprocessing import Lock

__author__ = 'Mattia Bertorello'
import math

import numpy as np

#https://github.com/zemasa/LT-codes
class Soliton:
    """/**
     * Creates new <code>Soliton</code> distribution object, according to the received parameters.
     * @param k Number of blocks to be encoded.
     * @param c Constant.
     * @param delta Admissible failure probability.
     * @throws IllegalArgumentException In case <code>k<=0, c<=0 or delta not in [0,1]</code>
     */"""

    def __init__(self, k, c, delta, random_generator):
        self.lock = Lock()
        i = 0
        if k > 0 and c > 0 and delta >= 0 and delta <= 1:
            k = self.k = np.int32(k)
            delta = self.delta = np.double(delta)
        else:
            raise Exception()
        self.random_generator = random_generator
        R = np.double(c * math.log(k / delta) * math.sqrt(k))

        rsd_max = np.int32(math.floor((k / R) + 0.5))

        if rsd_max > k:
            rsd_max = k

        R = k / rsd_max
        rsd_pmax = R * math.log(R / delta) / k

        beta = np.double(1)
        for i in range(1, rsd_max):
            beta += R / (i * k * 1.0)
        beta += rsd_pmax

        self.beta = beta
        self.rsd_pmax = rsd_pmax
        self.rsd_max = rsd_max
        self.R = R
        print "dmax = {:d} Overhead = {:f}".format(rsd_max, beta)

    def generate(self):
        self.lock.acquire()
        k = self.k
        try:
            v = self.random_generator.rand()
            rsd_pmax = self.rsd_pmax
            rsd_max = self.rsd_max
            rsd_beta = self.beta

            range = (rsd_pmax + 1.0 / (rsd_max * (rsd_max - 1))) / rsd_beta
            if v <= range:
                return rsd_max
            range += (1.0 / k + (1.0 / rsd_max)) / rsd_beta

            if v <= range:
                return 1

            for i in xrange(2, rsd_max):
                range += (1.0 / (rsd_max * i) + 1.0 / (i * (i - 1.0))) / rsd_beta
                if v <= range:
                    return i

            for i in xrange(rsd_max + 1, k - 1):
                range += (1.0 / (i * (i - 1.0))) / rsd_beta
                if v <= range:
                    return i
        finally:
            self.lock.release()
        return k

    def ideal_soliton(self):
        x = self.random_generator.rand()    # Uniform values in [0, 1)
        i = int(math.ceil(1 / x))             # Modified soliton distribution
        return i if i <= self.k else 1           # Correct extreme values to 1

    """	/**
     * Calculates the number of encoded packets required at the receiving end to ensure
     * that the decoding can run to completion, with probability at least 1-delta.
     * @return The number of encoded packets required at the receiving end to ensure
     * that the decoding can run to completion, with probability at least 1-delta.
     */"""

    def blocks_needed(self):
        return int(self.k * self.beta)


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    N = 64
    T = 10 ** 5 # Number of trials
    random_generator = np.random.RandomState()
    s = Soliton(N, 0.005, 0.005, random_generator) # soliton generator
    f = [0] * N                       # frequency counter
    for j in range(T):
        i = s.generate()
        f[i - 1] += 1

    T *= 1.0
    print("k\tFreq.\tExpected Prob\tObserved Prob\n");
    ris = [0] * N
    print f
    print("{:d}\t{:d}\t{:f}\t{:f}".format(1, f[0], 1.0 / N, f[0] / T))
    ris[0] = f[0] / T
    for k in range(2, N + 1):
        print("{:d}\t{:d}\t{:f}\t{:f}".format(k, f[k - 1], 1.0 / (k * (k - 1)), f[k - 1] / T))
        ris[k - 1] = f[k - 1] / T
    fig = plt.figure()
    plt.grid(True)
    print ris
    plt.plot(range(0, N), ris, 'r*', label='True Entropy')
    plt.show()
