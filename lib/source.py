__author__ = 'Mattia Bertorelo'

from math import log


class Symbols:
    def __init__(self, length):
        self.symbolLength = length

    def setLength(self, length):
        self.symbolLength = length

    def createDictionaryOccurrences(self, bitsGenerator):
        symbols = dict()
        totalOccurrences = 0
        for symbol in self.symbolsIterator(bitsGenerator):
            symbols[symbol] = symbols.get(symbol, 0) + 1
            totalOccurrences += 1

        symbolsDict = dict()
        symbolsDict['symbols'] = symbols
        symbolsDict['totalOccurrences'] = totalOccurrences
        symbolsDict['symbolLength'] = self.symbolLength
        print symbolsDict
        print len(symbolsDict['symbols'])
        return symbolsDict

    def symbolsIterator(self, bitsGenerator):
        symbol = ''
        for bit in bitsGenerator:
            if len(symbol) >= self.symbolLength:
                yield symbol
                symbol = ''
            symbol += str(bit)


class SourceProperties:
    def __init__(self, symbolsDict):
        self.symbolLength = symbolsDict['symbolLength']
        self.entropy = SourceProperties.simpleEntropy(symbolsDict['symbols'].itervalues(),
                                                      symbolsDict['totalOccurrences'])

    @staticmethod
    def simpleEntropy(symbolsOccurrences, totalSymbols):
        entropy = 0.0
        totalSymbols *= 1.0
        #print totalSymbols
        #TODO
        #print symbolsOccurrences
        for symbolOccurrences in symbolsOccurrences:
            symbolOccurrences *= 1.0
            #print symbolOccurrences
            px = symbolOccurrences / totalSymbols
            #print px
            entropy -= (px * log(px, 2))
            #print entropy

        return entropy

    @staticmethod
    def simple_entropy_with_probability(probabilities):
        entropy = 0.0
        for probability in probabilities:
            if probability == 1 or probability == 0:
                continue
            entropy -= (probability * log(probability, 2))

        return entropy

    def normalizeEntropy(self):
        return self.entropy / self.symbolLength

    def compressionRate(self):
        return self.symbolLength / self.entropy

