__author__ = 'Mattia Bertorelo'

from random import random


class RandomBitsGenerator:
    def __init__(self, probabilityOfZero, numberOfBits):
        self.probabilityOfZero = probabilityOfZero
        self.numberOfBits = numberOfBits

    def bits(self):
        for b in xrange(0, self.numberOfBits):
            number = random()
            if number < self.probabilityOfZero:
                yield 0
            else:
                yield 1


class FileBitsGenerator:
    def __init__(self, fileName):
        self.fileName = fileName
        self.bytes = []

    def bits(self):
        file = open(self.fileName, 'r')
        self.bytes = (ord(b) for b in file.read())
        for b in self.bytes:
            for i in xrange(8):
                yield (b >> i) & 1

