import math

__author__ = 'Mattia Bertorello'


def entropy(symbol_length, bits):
    """ Return the entropy calculate on a sequence of bits.
        It calculate with a symbol length = symbol_length

    @type symbol_length: int
    @param symbol_length: Length of the symbol to take, for example 3 symbol = 010
    @type bits: enumerate
    @param bits: Sequence of 1 or 0
    @rtype:   float
    @return:  the entropy not normalize of the sequence
    """
    symbols = dict()
    total_occurrences = 0
    symbol = ''
    # Calculation of the symbols in the sequence of bits
    for bit in bits:
        if symbol_length <= len(symbol):
            symbols[symbol] = symbols.get(symbol, 0) + 1
            total_occurrences += 1
            symbol = ''
        symbol += str(bit)

    # Estimate of the entropy
    entropy = 0.0
    total_occurrences *= 1.0

    for symbol in symbols.itervalues():
        px = symbol / total_occurrences
        entropy -= (px * math.log(px, 2))

    return entropy


def normalize_entropy(entropy, symbol_length):
    """
    @type entropy: float
    @param entropy:
    @type symbol_length: int
    @param symbol_length:
    @rtype: float
    @return:
    """
    return entropy / symbol_length


def compression_rate(entropy, symbol_length):
    """
    @type: entropy: float
    @param entropy:
    @type: symbol_length: int
    @param symbol_length:
    @rtype: float
    @return: Compression
    """
    try:
        return symbol_length / entropy
    except ZeroDivisionError:
        return 0


def binary_entropy(probability):
    return - (probability * math.log(probability, 2)) - (probability * math.log(probability, 2))


def theory_entropy(probability):
    entropy = 0.0
    for px in probability:
        entropy -= (px * math.log(px, 2))
    return entropy