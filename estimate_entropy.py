# -*- coding: UTF-8 -*-
import csv
from itertools import tee
import math
import itertools

import matplotlib.pyplot as plt

from lib import bit_generators as generators
from lib import entropy as e


__author__ = 'Mattia Bertorello'


def calculateEntropyAndCompressionRate(symbolLengthsList, bitsGenerator):
    generators = tee(bitsGenerator.bits(), len(symbolLengthsList))
    result = []
    for symbol_length, bits_generator in zip(symbolLengthsList, generators):
        entropy = e.entropy(symbol_length, bits_generator)
        normalize_entropy = e.normalize_entropy(entropy, symbol_length)
        compression_rate = e.compression_rate(entropy, symbol_length)

        print (symbol_length, normalize_entropy, compression_rate)
        result.append([normalize_entropy, compression_rate])
    return result


def drawPlotGenerateBit(data, data_zero, true_entropy, probability, fileName):
    fig = plt.figure(figsize=(15, 15))
    colors = itertools.cycle(['k', 'g', 'b', 'r']) #'c', 'm', 'y',

    ax1 = fig.add_subplot(211, title='Entropy & Compression Ratio with probability {0:.2}:'.format(probability))
    ax2 = fig.add_subplot(212, title='Entropy with probability {0:.2}:'.format(probability))
    color = colors.next()
    ax1.plot(data_zero, true_entropy, color=color, label='Theoretical Entropy')
    ax2.plot(data_zero, true_entropy, color=color, label='Theoretical Entropy')

    ax2.set_xlabel('Words length')

    for data_with__fixed_bits in data:
        data_with__fixed_probability = zip(*data_with__fixed_bits[1])
        entropy_label = 'Entropy with {0} bits'.format(data_with__fixed_bits[0])
        color = colors.next()
        ax1.plot(data_zero, data_with__fixed_probability[0], color=color, marker='o',
                 label=entropy_label)
        ax2.plot(data_zero, data_with__fixed_probability[0], color=color, marker='o',
                 label=entropy_label)
        ax1.plot(data_zero, data_with__fixed_probability[1], color=color, marker='o',
                 label='Compression Ratio with {0} bits'.format(data_with__fixed_bits[0]))

    ax2.set_ylim(top=1.2)
    ax1.legend(loc='best')
    ax2.legend(loc='best')
    ax1.grid(True, which='both')
    ax2.grid(True, which='both')
    fig.savefig(fileName + '_entropy.svg', format='svg', bbox_inches='tight')


def write_csv_of_theory_entropy(numbersOfBits, data, probability, path):
    title = ['Entropia Teorica con probabilità {0:.2}'.format(probability)]
    header = ['Symbol Length']
    header.append('True  Entropy')
    for numberOfBits in numbersOfBits:
        header.append('Entropy {0} bits'.format(numberOfBits))
        header.append('Compression Ratio  {0} bits'.format(numberOfBits))

    data2 = []

    def flat_value(iterable):
        for elem0 in iterable:
            flat_ris = []
            for elem in elem0:
                try:
                    some_object_iterator = iter(elem)
                    flat_ris += elem
                except TypeError, te:
                    flat_ris += [elem]
            data2.append(["{0:.5f}".format(x) for x in flat_ris])
        data2

    flat_value(data)
    data_to_write = title + [header] + data2

    with open(path + '.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(data_to_write)


def theory_entropy(path, symbolLengthList):
    probabilities = [0.5, 0.6, 0.8, 0.9, 0.99]
    numbersOfBits = [100, 1000, 10000]#,10000, 10000000]
    for probability in probabilities:
        data = []
        data2 = []
        for numberOfBits in numbersOfBits:
            bitsGenerator = generators.RandomBitsGenerator(probability, numberOfBits + 1)
            aux = calculateEntropyAndCompressionRate(symbolLengthList, bitsGenerator)
            data.append((numberOfBits, aux, ))
            data2.append(aux)
        entropy = e.theory_entropy([probability, 1 - probability])
        true_entropy = [entropy] * len(symbolLengthList)
        csv_data = zip(symbolLengthList, true_entropy, *data2)
        write_csv_of_theory_entropy(numbersOfBits, csv_data, probability,
                                    path + "fixed_probability_" + str(probability))
        drawPlotGenerateBit(data, symbolLengthList, true_entropy,
                            probability, path + "fixed_probability_" + str(probability))


def drawPlot(data, data_zero, true_entropy, file_type, file_name):
    fig = plt.figure(figsize=(15, 15))
    colors = itertools.cycle(['k', 'g', 'b', 'r', ]) #'c', 'm', 'y',

    ax1 = fig.add_subplot(211, title='Entropy & Compression Ratio file type: {}'.format(file_type))
    ax2 = fig.add_subplot(212, title='Entropy with probability file type: {}'.format(file_type))
    color = colors.next()
    ax1.plot(data_zero, true_entropy, color=color, label='Zip Compression Ratio')

    ax2.set_xlabel('Words length')

    for file in data:
        data_with__fixed_probability = zip(*file[1])
        entropy_label = 'Entropy with {0} bits'.format(file[0])
        color = colors.next()
        ax1.plot(data_zero, data_with__fixed_probability[0], color=color, marker='o',
                 label=entropy_label)
        ax2.plot(data_zero, data_with__fixed_probability[0], color=color, marker='o',
                 label=entropy_label)
        ax1.plot(data_zero, data_with__fixed_probability[1], color=color, marker='o',
                 label='Compression Ratio with {0} bits'.format(file[0]))

    ax2.set_ylim(top=1.2)
    ax1.legend(loc='best')
    ax2.legend(loc='best')
    ax1.grid(True, which='both')
    ax2.grid(True, which='both')
    fig.savefig(file_name + '_entropy.svg', format='svg', bbox_inches='tight')


def file_entropy(path, symbolLengthList):
    files_path = "test_files/"
    #files = [f for f in listdir(files_path) if isfile(join(files_path, f))]
    groups_files = [('image', ['image.jpg', 'image.jpg.zip'], 0.97),
                    ('kernel log', ['kernel_log', 'kernel_log.zip'], 6.4),
                    ('rfc', ['rfc_text', 'rfc_text.zip'], 3.54),
                    ('video H254', ['video_264.mp4', 'video_264.mp4.zip'], 1.01),
                    ('audio wav', ['audio.wav', 'audio.wav.zip'], 1.13),
                    ('audio mp3', ['audio.mp3', 'audio.mp3.zip'], 1.01)]
    for group_files in groups_files:
        data = []
        data2 = []
        for file in group_files[1]:
            bitsGenerator = generators.FileBitsGenerator(files_path + file)
            aux = calculateEntropyAndCompressionRate(symbolLengthList, bitsGenerator)
            data.append((file, aux, ))
            data2.append(aux)
        zip_compression_ratio = [group_files[2]] * len(symbolLengthList)
        #csv_data = zip(symbolLengthList, true_entropy, *data2)
        #write_csv_of_theory_entropy(numbersOfBits, csv_data, probability,
        #                            path + "fixed_probability_" + str(probability))
        #drawPlot(data, symbolLengthList, zip_compression_ratio, group_files[0], path + group_files[0])


# Contare le sequenze tipiche e calcolare l'entropia
# Prendere il file di testo e critografarlo e vedere cosa succede
#numberOfBits = 10000
#bitsGenerator = RandomBitsGenerator(0.5, numberOfBits)
if __name__ == "__main__":
    #TODO Per i File zippati inserire la linea teorica di zip e vedere cosa noi riusciamo a fare
    path = 'graphs/es1_entropy/'
    maxSymbolLength = 16
    symbolLengthList = [2 ** i for i in xrange(0, int(math.log(maxSymbolLength, 2) + 1))]

    #theory_entropy(path, symbolLengthList)
    file_entropy(path, symbolLengthList)